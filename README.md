Tumblr Metro Theme
====================
This Theme has a Metro Design, with 'live' tiles for each Post, it is currently under development but will eventially be responsive and look very similar to the Windows Metro UI design standard.

1. Copy all the code in theme.html into your custom HTML section.
2. Under 'Advanced Options> Add Custom CSS' Paste the contents of the custom_styles.css file.

Apply and Save and you now have a metro themed tumblr page that scrolls sideways.

NOTES
================
Libraries in use:
Masonry JS for the layout
http://masonry.desandro.com/

Fancybox Lightbox For lightbox effect on the images.
http://fancyapps.com/fancybox/

Tile Colors Are hand coded:
```JavaScript
$(document).ready(function(){
            $('.text').each(function(){
                var colors = ["FF0097", "#00ABA9", "#8CBF26", "#A05000", "#E671B8", "#F09609", "#1BA1E2", "#E51400", "#339933"];                
            var rand = Math.floor(Math.random()*colors.length); 
                $(this).css("background-color", colors[rand]);
            });
            $('.photo').each(function(){
                var colors = ["FF0097", "#00ABA9", "#8CBF26", "#A05000", "#E671B8", "#F09609", "#1BA1E2", "#E51400", "#339933"];                
            var rand = Math.floor(Math.random()*colors.length); 
                $(this).css("background-color", colors[rand]);
            });
            $('.video').each(function(){
                var colors = ["FF0097", "#00ABA9", "#8CBF26", "#A05000", "#E671B8", "#F09609", "#1BA1E2", "#E51400", "#339933"];                
            var rand = Math.floor(Math.random()*colors.length); 
                $(this).css("background-color", colors[rand]);
            });
        });
```

To Be done:

[X] Lightbox Images.
[ ] potentially Tile Size Options
[ ] Footer Information/Branding
[ ] Header Navigation/Profile Info
[ ] Comment code for readability
[X] Fix Colors (Use 'for each' jQuery Loop) *DONE*
